# mini-proj1

## Descrição
```
Mini projeto que tem como objetivo aprender e consolidar o conceito SPA. 
O site a funcionar numa única página corresponderá ao seu currículo vitae, a partir de uma página index.html. 

O site deve ter a seguinte:
Quem Sou Eu?
O que faço?
Hobbies

No rodapé da página index.html, bem como nas restantes páginas criadas, deve surgir o rodapé com a indentidade virtual 
composta por um conjunto de icones de redes sociais populares (ex. facebook, Google +, Twitter, Linkedin, etc.)
```

## Project setup
```
yarn global add @vue/cli @vue/cli-service-global
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```
