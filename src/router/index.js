import Vue from 'vue'
import VueRouter from "vue-router";
import WhoAmI from "@/components/WhoAmI";
import Work from "@/components/Work";
import Hobbies from "@/components/Hobbies";

Vue.use(VueRouter)

export default new VueRouter({
    routes: [
        { "path": "/", "component": WhoAmI },
        { "path": "/work", "component": Work },
        { "path": "/hobbies", "component": Hobbies }
    ]
});